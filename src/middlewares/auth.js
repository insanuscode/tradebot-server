const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
  const authHeader = req.headers.authorization;

  if (!authHeader)
    return res
      .status(401)
      .json({ error: 'Requisição não authorizada. Token não informado.' });

  const parts = authHeader.split(' ');

  if (!parts.lenth === 2)
    return res.status(401).json({ error: 'Token mal formatado.' });

  const [scheme, token] = parts;

  if (!/^Bearer$/i.test(scheme))
    return res.status(401).json({ error: 'Token mal formatado.' });

  jwt.verify(token, process.env.TOKEN_SECRET, (err, decoded) => {
    if (err) return res.status(401).json({ errror: 'Token inválido' });
    console.log('Exp === ', decoded.exp);

    req.userId = decoded.id;
    return next();
  });
};
