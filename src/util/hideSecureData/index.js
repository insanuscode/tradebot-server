module.exports = function loginHide() {
  this.password = undefined;
  this.passwordResetExpires = undefined;
  this.passwordResetToken = undefined;
  this.key = undefined;
  this.secureKey = undefined;
};
