module.exports = {
  candlesNumber: 100,
  // sri
  rsiperiod: 14,

  // macd
  macdFastPeriod: 5,
  macdSlowPeriod: 8,
  macdSignalPeriod: 3,
  macdSimpleMAOscillator: false,
  macdSimpleMASignal: false,

  // bb
  bbPeriod: 14,
  bbStdDev: 2,

  // sma
  smaPeriod: 14,

  // stochastic
  stochasticPeriod: 14,
  stochasticSignalPeriod: 3,
};
