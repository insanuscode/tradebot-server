// expor default config.
module.exports = [
  // RSI
  {
    tag: 'BUY_OVER',
    value: 2,
    description: 'Cruza 70% pra cima',
    group: 'RSI',
  },
  {
    tag: 'EXPENSIVE',
    value: -1,
    description: 'Acima dos 70%',
    group: 'RSI',
  },
  {
    tag: 'HIGH_BEARISH_POSSIBILITY',
    value: -3,
    description: 'Cruza 50% pra baixo',
    group: 'RSI',
  },
  {
    tag: 'MEDIUM_BEARISH_POSSILITY',
    value: -2,
    description: 'Cruza 70% pra baixo',
    group: 'RSI',
  },
  {
    tag: 'BEARISH_POSSIBILITY',
    value: -4,
    description: 'Entre 30% e 50%',
    group: 'RSI',
  },
  {
    tag: 'HIGH_BULLISH_POSSIBILITY',
    value: 3,
    description: 'Cruza 50% pra cima',
    group: 'RSI',
  },
  {
    tag: 'SELL',
    value: -2,
    description: 'Cruza 30% pra baixo',
    group: 'RSI',
  },
  {
    tag: 'BULLISH_POSSIBILITY',
    value: 4,
    description: 'Entre 50% e 70%',
    group: 'RSI',
  },
  {
    tag: 'CHEAP_LOW_RISK',
    value: 2,
    description: 'Cruza 30% pra cima',
    group: 'RSI',
  },
  { tag: 'CHEAP', value: 1, description: 'Abaixo dos 30%', group: 'RSI' },
  {
    tag: 'BUY_UNDER',
    value: 3,
    description: 'V2 > 30% e V2 > V1',
    group: 'RSI',
  },

  // stochastic
  {
    tag: 'BUY',
    value: 3,
    description: '(K ou D ≤ 20) e (K cruza D pra cima)',
    group: 'STOCHASTIC',
  },
  {
    tag: 'SELL',
    value: -3,
    description: '(K ou D ≥ 80) e (K cruza D pra baixo)',
    group: 'STOCHASTIC',
  },
  {
    tag: 'CHEAP',
    value: 2,
    description: 'K ou D ≤ 20',
    group: 'STOCHASTIC',
  },
  {
    tag: 'EXPENSIVE',
    value: -2,
    description: 'K ou D ≥ 80',
    group: 'STOCHASTIC',
  },
  {
    tag: 'BULLISH_POSSIBILITY',
    value: 0,
    description: 'K ou D ≥ 80',
    group: 'STOCHASTIC',
  },
  {
    tag: 'BEARISH_POSSIBILITY',
    value: 0,
    description: 'K e D entre 20 e 50',
    group: 'STOCHASTIC',
  },
  {
    tag: 'MIDDLE_HIGH_CROSS_UP',
    value: 2,
    description: '(K e D entre 50 e 80) e k cruza D pra cima',
    group: 'STOCHASTIC',
  },
  {
    tag: 'MIDDLE_HIGH_CROSS_DOWN',
    value: -1,
    description: '(K e D entre 50 e 80) e K cruza D pra baixo',
    group: 'STOCHASTIC',
  },
  {
    tag: 'MIDDLE_LOW_CROSS_UP',
    value: 1,
    description: '(K e D entre 50 e 20) e K cruza D pra cima',
    group: 'STOCHASTIC',
  },
  {
    tag: 'MIDDLE_LOW_CROSS_DOWN',
    value: -2,
    description: '(K e D entre 50 e 20) e K cruza D pra baixo',
    group: 'STOCHASTIC',
  },

  // MACD
  {
    tag: 'SELL',
    value: -1,
    description: 'Macd cruza sinal pra baixo',
    group: 'MACD',
  },
  {
    tag: 'BUY',
    value: 1,
    description: 'Macd cruza sinal pra cima',
    group: 'MACD',
  },
  {
    tag: 'CROSS_ZERO_UP',
    value: 2,
    description: 'Macd cruza zero pra cima',
    group: 'MACD',
  },
  {
    tag: 'CROSS_ZERO_DOWN',
    value: -2,
    description: 'Macd cruza zero pra baixo',
    group: 'MACD',
  },

  // EMA
  {
    tag: 'BEARISH_POSSIBILITY',
    value: 50,
    description: 'M7 cruza M21 pra cima',
    group: 'EMA',
  },
  {
    tag: 'BULLISH_POSSIBILITY',
    value: -50,
    description: 'M7 cruza M21 pra baixo',
    group: 'EMA',
  },

  // BB
  {
    tag: 'HIGH_BEARISH_POSSIBILITY',
    value: -3,
    description: 'Cruza meio pra baixo',
    group: 'BB',
  },
  {
    tag: 'MEDIUM_BEARISH_POSSILITY',
    value: -2,
    description: 'Cruza topo pra baixo',
    group: 'BB',
  },
  {
    tag: 'BEARISH_POSSIBILITY',
    value: -2,
    description: 'Entre fundo e meio',
    group: 'BB',
  },
  {
    tag: 'HIGH_BULLISH_POSSIBILITY',
    value: 3,
    description: 'Cruza meio pra cima',
    group: 'BB',
  },
  {
    tag: 'MEDIUM_BULLISH_POSSIBILITY',
    value: 2,
    description: 'Cruza topo pra cima',
    group: 'BB',
  },
  {
    tag: 'BULLISH_POSSIBILITY',
    value: 2,
    description: 'Entre topo e meio',
    group: 'BB',
  },
  {
    tag: 'NULL_UPPER',
    value: 1,
    description: 'Acima do topo',
    group: 'BB',
  },
  {
    tag: 'NULL_LOWER',
    value: -1,
    description: 'Abaixo do topo',
    group: 'BB',
  },

  // Tendencies
  // Media Tendencies
  {
    tag: 'UP',
    value: 1,
    description: 'Maior que anterior',
    group: 'MEDIA_TENDENCIES',
  },
  {
    tag: 'DOWN',
    value: -1,
    description: 'Menor que anterior',
    group: 'MEDIA_TENDENCIES',
  },

  // Price EMA Tendencies 25
  {
    tag: 'UP',
    value: 10,
    description: 'Acima da media(ema25)',
    group: 'PRICE_EMA_TENDENCIES_25',
  },
  {
    tag: 'DOWN',
    value: -10,
    description: 'Abaixo da media(ema25)',
    group: 'PRICE_EMA_TENDENCIES_25',
  },
  {
    tag: 'UP_FLAG1',
    value: 40,
    description: 'Rompimento pra cima',
    group: 'PRICE_EMA_TENDENCIES_25',
  },
  {
    tag: 'DOWN_FLAG1',
    value: -40,
    description: 'Rompimento pra baixo',
    group: 'PRICE_EMA_TENDENCIES_25',
  },
  {
    tag: 'UP_CONFIRMED',
    value: 60,
    description: 'Rompimento com duas confirmações alta',
    group: 'PRICE_EMA_TENDENCIES_25',
  },
  {
    tag: 'DOWN_CONFIRMED',
    value: -60,
    description: 'Rompimento com duas confirmações de baixa',
    group: 'PRICE_EMA_TENDENCIES_25',
  },

  // Price EMA Tendencies 7
  {
    tag: 'UP',
    value: 5,
    description: 'Acima da media(ema7)',
    group: 'PRICE_EMA_TENDENCIES_7',
  },
  {
    tag: 'DOWN',
    value: -5,
    description: 'Abaixo da media(ema7)',
    group: 'PRICE_EMA_TENDENCIES_7',
  },
  {
    tag: 'UP_FLAG1',
    value: 20,
    description: 'Rompimento pra cima',
    group: 'PRICE_EMA_TENDENCIES_7',
  },
  {
    tag: 'DOWN_FLAG1',
    value: -20,
    description: 'Rompimento pra baixo',
    group: 'PRICE_EMA_TENDENCIES_7',
  },
  {
    tag: 'UP_CONFIRMED',
    value: 10,
    description: 'Rompimento com duas confirmações alta',
    group: 'PRICE_EMA_TENDENCIES_7',
  },
  {
    tag: 'DOWN_CONFIRMED',
    value: -10,
    description: 'Rompimento com duas confirmações de baixa',
    group: 'PRICE_EMA_TENDENCIES_7',
  },
];
