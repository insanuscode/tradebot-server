const nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');
const path = require('path');

var transport = nodemailer.createTransport({
  host: process.env.MAIL_HOST,
  port: process.env.MAIL_PORT,
  auth: {
    user: process.env.MAIL_USER,
    pass: process.env.MAIL_PASSWORD,
  },
});

transport.use(
  'compile',
  hbs({
    viewEngine: {
      extname: '.handlebars',
      partialsDir: './src/util/mailer/mail',
    },
    viewPath: path.resolve('./src/util/mailer/mail'),
    extName: '.handlebars',
  })
);

module.exports = transport;
