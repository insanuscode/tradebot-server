const axios = require('axios').default;

async function getExchangeInfo(currencies) {
  const host = process.env.BINANCE_HOST;

  // const currencies = JSON.stringify(symbols).trim();
  console.log('Curencies', currencies);
  const result = await axios.get(`${host}/api/v3/exchangeInfo`, {
    params: {
      symbols: currencies
        ? JSON.stringify(currencies).replace(' ', '')
        : undefined,
    },
  });

  return result.data.symbols;
}

module.exports = getExchangeInfo;
