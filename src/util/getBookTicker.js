const axios = require('axios').default;

module.exports = async (symbol, interval, limit) => {
  const host = process.env.BINANCE_HOST;

  try {
    const result = await axios.get(`${host}/api/v3/ticker/bookTicker`);

    // console.log("Candles: ", result.data);
    return result.data;
  } catch (error) {
    console.error(error);
    return [];
  }
};
