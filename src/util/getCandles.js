const axios = require('axios').default;

const formatCandles = async (candles) => {
  const data = await candles
    .map((d) => d.map((r) => Number(r)))
    .reduce((acc, cur, i) => {
      // console.log(`ARR[${i}]`, "ACC", acc, "CUR", cur);
      acc['openTime'] = acc['openTime']
        ? [...acc['openTime'], cur[0]]
        : [cur[0]];
      acc['open'] = acc['open'] ? [...acc['open'], cur[1]] : [cur[1]];
      acc['high'] = acc['high'] ? [...acc['high'], cur[2]] : [cur[2]];
      acc['low'] = acc['low'] ? [...acc['low'], cur[3]] : [cur[3]];
      acc['close'] = acc['close'] ? [...acc['close'], cur[4]] : [cur[4]];
      acc['volume'] = acc['volume'] ? [...acc['volume'], cur[5]] : [cur[5]];
      acc['closeTime'] = acc['closeTime']
        ? [...acc['closeTime'], cur[6]]
        : [cur[6]];
      acc['quote'] = acc['quote'] ? [...acc['quote'], cur[7]] : [cur[7]];
      acc['numberTraders'] = acc['numberTraders']
        ? [...acc['numberTraders'], cur[8]]
        : [cur[8]];
      acc['buyBase'] = acc['buyBase'] ? [...acc['buyBase'], cur[9]] : [cur[9]];
      acc['buyQuote'] = acc['buyQuote']
        ? [...acc['buyQuote'], cur[10]]
        : [cur[10]];
      acc['ignore'] = acc['ignore'] ? [...acc['ignore'], cur[11]] : [cur[11]];

      return acc;
    }, {});

  // console.log("Candles depois da formatação:", data);
  return data;
};

const getCandles = async (symbol, interval, limit) => {
  const host = process.env.BINANCE_HOST;

  try {
    const result = await axios.get(`${host}/api/v3/klines`, {
      params: {
        symbol: symbol,
        interval: interval || '1m',
        limit: limit || 50,
      },
    });

    // console.log('Candles: ', result.data);
    return formatCandles(result.data);
  } catch (error) {
    console.error(error);
    return [];
  }
};

module.exports = getCandles;
