const crypto = require('crypto');

function buildSign(params, secret) {
  params = new URLSearchParams(params).toString();
  return crypto.createHmac('sha256', secret).update(params).digest('hex');
}

module.exports = buildSign;
