const { default: axios } = require('axios');
const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient();

module.exports = async (limit) => {
  // const coin = String(stablecoin).toUpperCase();

  let avalableCurrencies = await prisma.meta.findFirst({
    where: { description: 'VALID_PAIRS' },
  });
  avalableCurrencies = JSON.parse(avalableCurrencies.value);

  try {
    // `${process.env.MARKETCAP_HOST}/v1/cryptocurrency/map`,
    const result = await axios.get(
      'https://pro-api.coinmarketcap.com/v1/cryptocurrency/map',
      {
        params: {
          limit: limit + 30,
          sort: 'cmc_rank',
        },
        headers: {
          'X-CMC_PRO_API_KEY': 'fbaacd0e-aa07-4dd8-83c4-483025baa3ed',
        },
      }
    );
    if (result.data['status']['error_code'] !== 0)
      throw new Error(`Bad Request cod ${result.data['status']['error_code']}`);

    const symbols = result.data.data
      .map((d) => d['symbol'])
      .filter((f) => f !== 'USDT');
    return symbols;
  } catch (error) {
    console.error('Algo errado com getSymbols: ', error);
  }
};
