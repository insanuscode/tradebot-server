const router = require('express').Router();
const { PrismaClient } = require('@prisma/client');
const auth = require('../../middlewares/auth');

const prisma = new PrismaClient();

router.use(auth);

router.get('/', async (req, res) => {
  try {
    const configs = await prisma.ia.findMany({ where: { id: req.userId } });

    res.json(configs);
  } catch (e) {
    res.status(400).json({ error: 'algo deu errado' });
  }
});

router.post('/', async (req, res) => {
  const {
    rank,
    stableCoin,
    coinNumber,
    spread,
    profitPercent,
    time,
    lossPercent,
    loop,
    amount,
  } = req.body;

  try {
    const data = await prisma.ia.create({
      data: {
        userId: req.userId,
        rank: Math.trunc(Number(rank)),
        stableCoin,
        coinNumber: Math.trunc(Number(coinNumber)),
        spread: Math.trunc(Number(spread)),
        profitPercent: Number(profitPercent),
        time: Math.trunc(Number(time)),
        lossPercent: Number(lossPercent),
        loop: Math.trunc(Number(loop)),
        amount: amount,
      },
    });

    console.log('IA', data);
    res.json(data);
  } catch (e) {
    res.status(400).json({ error: 'Algum dado errado' });
  }
});

router.delete('/', async (req, res) => {
  const { id } = req.body;

  try {
    const deleted = await prisma.ia.delete({
      where: { userId: req.userId, id },
    });
    res.json(deleted);
  } catch (e) {
    res.status(400).json({ error: 'Algo deu errado.' });
  }
});

module.exports = (app) => {
  app.use('/ia', router);
};
