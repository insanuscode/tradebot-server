const { EMA } = require('technicalindicators');

function calc(candles, period) {
  const input = {
    period,
    values: candles['close'],
  };

  return EMA.calculate(input);
}

function analysis(candles, ema7, ema21) {
  // const offset = ema7.length - ema21.length;

  const controle = ema7.length - ema21.length;
  const offset = candles['close'].length - ema21.length - 1;

  console.log(ema7.length, ema21.length);
  const analize = ema21.map((v, i, arr) => {
    if (i < 2) return null;

    const idx7 = controle + i;

    // Does M7 cross up M21?
    if (
      ema7[idx7 - 2] < arr[i - 2] &&
      ema7[idx7 - 1] >= arr[i - 1] &&
      ema7[idx7] > v
    )
      return 'BEARISH_POSSIBILITY';
    // return {
    //   closeTime: candles['openTime'][offset + i],
    //   close: candles['close'][offset + i],
    //   result: 'BEARISH_POSSIBILITY',
    // }; // invert

    // Does M7 cross down M21?
    if (
      ema7[idx7 - 2] > arr[i - 2] &&
      ema7[idx7 - 1] <= arr[i - 1] &&
      ema7[idx7] < v
    )
      return 'BULLISH_POSSIBILITY';
    // return {
    //   closeTime: candles['openTime'][offset + i],
    //   close: candles['close'][offset + i],
    //   result: 'BULLISH_POSSIBILITY',
    // }; // invert
  });

  return analize;
}

function mediaMovelExponencial(candles) {
  const ema7 = calc(candles, 7);
  const ema21 = calc(candles, 21);

  const analyze = analysis(candles, ema7, ema21);

  return {
    ema7,
    ema21,
    analyze,
  };
}

module.exports = { analyze: mediaMovelExponencial, calc };
