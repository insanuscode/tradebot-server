const { compareSync } = require('bcryptjs');

function getExtremes(candles) {
  const bars = candles.close.length;

  // Reverse the candles we need the last candles takes place at index 0
  const cand = Object.keys(candles).reduce((prev, key) => {
    const temp = [...candles[key]];
    prev[key] = temp.reverse();
    return prev;
  }, {});
  let { close, open } = cand;
  let price,
    index = 0;
  let min = [];
  let max = [];

  price = close[index];
  index = 1; // primeiro candles verificado.

  // verifica se precisa buscar um topo primeiro
  const isStartedUp = close[0] > close[1]; //|| close[0] > close[2];

  while (index < bars) {
    const lastPosition = bars - 1;
    let currentMax1 = lastPosition,
      currentMax2 = lastPosition,
      currentMin1 = lastPosition,
      currentMin2 = lastPosition;

    // Se isStartUp for True significa que o primeiro extremo
    // será um fundo, caso contrario será um topo.
    // e isso será intercalado ao decorer de todos os pontos extremos;
    // ou seja, isStartUp for True buscamos: fundo, topo, fundo, topo. ate acabar os candles analisados
    if (isStartedUp) {
      index = minimum(index, bars, price);
      currentMin1 = index;
      price = Math.min(open[index], close[index]);
      index++;

      // Max's
      index = maximum(index, bars, price);
      currentMax1 = index;
      price = Math.max(open[index], close[index]);
      index++;
      currentMin2 = minimum(index, bars, price);

      const lastMinFound = check_min(currentMin1, currentMax1);
      const lastMaxFound = check_max(currentMax1, currentMin2);
      lastMinFound !== bars - 1 && min.push(lastMinFound);
      lastMaxFound !== bars - 1 && max.push(lastMaxFound);
    } else {
      // max
      index = maximum(index, bars, price);
      currentMax1 = index;
      price = Math.max(open[index], close[index]);
      index++;

      // Max's
      index = minimum(index, bars, price);
      currentMin1 = index;
      price = Math.min(open[index], close[index]);
      index++;
      currentMax2 = maximum(index, bars, price);

      const lastMaxFound = check_max(currentMax1, currentMin1);
      const lastMinFound = check_min(currentMin1, currentMax2);
      lastMaxFound !== bars - 1 && max.push(lastMaxFound);
      lastMinFound !== bars - 1 && min.push(lastMinFound);
    }
  }

  // Verify where is the low price/bottom of wave.
  function minimum(a, b, price0) {
    let i,
      e = b - 1;

    for (i = a; i < b; i++) {
      const lowPrice = Math.min(open[i], close[i]);
      const nextLowPrice = Math.min(open[i + 1], close[i + 1]);
      if (lowPrice < price0 && lowPrice < nextLowPrice) {
        e = i;
        break;
      }
    }

    return e;
  }

  function maximum(a, b, price1) {
    let i,
      e = bars - 1;

    for (
      i = a;
      i < b;
      i++ // Procure o pico dentro do intervalo especificado pelos parâmetros a e b
    ) {
      const highPrice = Math.max(open[i], close[i]);
      const nextHighPrice = Math.max(open[i + 1], close[i + 1]);
      if (highPrice > price1 && highPrice > nextHighPrice) {
        // Definir o pico mais próximo, após o preço começar a cair
        e = i;
        break;
      }
    }
    return e;
  }

  // a função para verificar e corrigir a posição inferior dentro do intervalo especificado
  function check_min(a, b) {
    let i,
      c = a;
    for (
      i = a + 1;
      i < b;
      i++ // Ao procurar o fundo, todas as barras especificadas pelo intervalo são verificadas
    ) {
      const valueI = Math.min(open[i], close[i]);
      const valueA = Math.min(open[a], close[a]);
      const valueC = Math.min(open[c], close[c]);
      if (valueI < valueA && valueI < valueC)
        // Se o fundo localizado mais baixo for encontrado
        c = i; // O local do fundo é redefinido
    }
    return c;
  }

  //--- A função para verificar e corrigir a posição de pico dentro do intervalo especificado
  function check_max(a, b) {
    //  double high[],low[];
    //  ArraySetAsSeries(high,true);
    //  int copied7=Copyhigh(Symbol(),0,0,bars+1,high);
    let i,
      d = a;

    for (
      i = a + 1;
      i < b;
      i++ // Ao procurar o fundo, todas as barras especificadas pelo intervalo são verificadas
    ) {
      const valueI = Math.max(open[i], close[i]);
      const valueA = Math.max(open[a], close[a]);
      const valueD = Math.max(open[d], close[d]);
      if (valueI > valueA && valueI > valueD)
        // Se o pico localizado mais alto for encontrado
        d = i; // A localização do pico é redefinida
    }
    return d;
  }

  function getCandlesAsObject(indexiesArray, candleProps, candles) {
    return indexiesArray.map((index) => {
      const temp = {};
      candleProps.forEach((prop) => {
        temp[prop] = candles[prop][index];
      });

      return temp;
    });
  }

  const candkeys = Object.keys(cand);
  const minimos = getCandlesAsObject(min, candkeys, cand);
  const maximos = getCandlesAsObject(max, candkeys, cand);

  let extremes = {
    min: minimos.map((m) => ({ ...m, type: 'bottom' })),
    max: maximos.map((m) => ({ ...m, type: 'top' })),
  };

  extremes = [extremes.min, extremes.max].flat();
  extremes = extremes.sort((a, b) => {
    if (a.openTime < b.openTime) return -1;
    if (a.openTime === b.openTime) return 0;
    return 1;
  });

  return extremes;
}

function analyzeTendencies(extremes) {
  function max(e) {
    if (e === undefined) console.log('E = ', e);
    return Math.max(e.close, e.open);
  }

  function min(e) {
    return Math.min(e.close, e.open);
  }

  function peformAnalyze(tops, bots) {
    const last = tops.length - 1;
    if (
      max(tops[last]) > max(tops[last - 1]) &&
      max(tops[last - 1]) > max(tops[last - 2])
    ) {
      if (
        min(bots[last]) > min(bots[last - 1]) &&
        min(bots[last - 1]) > min(bots[last - 2])
      ) {
        return 'UP';
      }
    }

    if (
      max(tops[last]) < max(tops[last - 1]) &&
      max(tops[last - 1]) < max(tops[last - 2])
    ) {
      if (
        min(bots[last]) < min(bots[last - 1]) &&
        min(bots[last - 1]) < min(bots[last - 2])
      ) {
        return 'DOWN';
      }
    }

    return false;
  }

  if (extremes.length < 8) return null;

  let lastExtremes = extremes.slice(-8);

  let tops = lastExtremes.filter((v) => v.type.toLowerCase() === 'top');
  let bots = lastExtremes.filter((v) => v.type.toLowerCase() === 'bottom');

  let analyze = peformAnalyze(tops, bots);

  // if (tops[-3] < tops[-4] && tops[-2] > tops[-3] && tops[-1] > tops[-2])
  // let extremePairs = [];
  // for (let i = 0; i < lastExtremes.length; i += 2) {
  //   const temp = [lastExtremes[i], lastExtremes[i + 1]];
  //   extremePairs.push(temp);
  // }

  // console.log(extremePairs);

  // if (lastExtremes[-1].type.toLoweCase() === top){
  //   lastExtreme[-8]
  // }

  return {
    analyze,
  };
}

module.exports = { getExtremes, analyzeTendencies };
