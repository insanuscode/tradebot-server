const router = require('express').Router();
const { PrismaClient } = require('@prisma/client');
const getIndicators = require('./getIndicators');
const auth = require('../../middlewares/auth');
const getCurrencyRank = require('./getCurrencyRank');
const calcScore = require('./score');

const prisma = new PrismaClient();
router.use(auth);
router.get('/', async (req, res) => {
  // Parametros necessarios.
  // Ia.id, userId,
  try {
    const { ia } = req.body;
    // Fetch datas
    const indicatorsConfig = await prisma.indicatorsConfig.findFirst();
    const iaConfig = await prisma.ia.findFirst({
      where: { userId: req.userId, id: ia },
    });

    if (!iaConfig) throw new Error('Configuração do robô não encontrada.');

    let currencyRank = await getCurrencyRank(iaConfig);

    let currencyAnalysis = await getIndicators(
      iaConfig,
      indicatorsConfig,
      currencyRank
    );

    const flags = await prisma.scoreConfigurations.findMany();

    console.log('info', currencyAnalysis);
    currencyAnalysis = currencyAnalysis.map((coinResume) => {
      // if (!Object.keys(coinResume).includes('indicators'))
      //   console.log(coinResume);
      const newResume = calcScore(coinResume, flags);
      // return {
      //   ...coinResume,
      //   indicators: newResume.indicators,
      //   score: newResume.score,
      // };
      return newResume;
    });

    res.json(
      currencyAnalysis.sort((a, b) => {
        if (a.score > b.score) return -1;
        if (a.score < b.score) return 1;
        return 0;
      })
    );
  } catch (e) {
    console.log(e);
    res.status(400).json({ error: 'algo deu errado' });
  }
});

require('./configuration')(router);

module.exports = (app) => {
  app.use('/indicators', router);
};
