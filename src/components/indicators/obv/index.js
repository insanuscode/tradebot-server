const { prisma } = require('.prisma/client');
const { OBV } = require('technicalindicators');

const config = {
  deltaPercent: 0.0003,
};
// On Balance Volume (OBV)
function calc(candles) {
  // const input = {
  //   close: candles['close'],
  //   volume: candles['volume'],
  // };

  // return OBV.calculate(input);

  let accumulated = 0;
  const result = candles['close'].map((close, idx, arr) => {
    if (idx === 0) {
      accumulated = 0;
      return accumulated;
    }

    const prev = arr[idx - 1];
    const current = close;
    const volume = candles['volume'][idx];

    if (prev < current) {
      accumulated += volume;
      return accumulated;
    }
    if (prev > current) {
      accumulated -= volume;
      return accumulated;
    }
    return accumulated;
  });

  return result;
}

function minimun(a, b, value, delta, obvs) {
  let i,
    e = b;

  let v = value;
  for (i = a; i > b; i--) {
    if (obvs[i] < v && obvs[i - 1] > obvs[i]) {
      e = i;
      break;
    }
  }

  return e;
}

function maximun(a, b, value, delta, obvs) {
  let i,
    e = b;

  let v = value;
  for (i = a; i > b; i--) {
    if (obvs[i] > v && obvs[i - 1] < obvs[i]) {
      e = i;
      break;
    }
  }

  return e;
}

function check_min(a, b, obvs) {
  let i,
    c = a;

  for (i = a - 1; i > b; i--) {
    if (obvs[i] < obvs[a] && obvs[i] < obvs[c]) {
      c = i;
    }
  }

  return c;
}

function check_max(a, b, obvs) {
  let i,
    c = a;

  for (i = a - 1; i > b; i--) {
    if (obvs[i] > obvs[a] && obvs[i] > obvs[c]) c = i;
  }

  return c;
}

function analisis(candles, obvs) {}

function obv(candles) {
  const obvs = calc(candles);
  // const analize = analisis(candles, obvs);

  console.log('Valor', obvs[0], 'tipo', typeof obvs[0]);
  const max = Math.max(...obvs);
  const min = Math.min(...obvs);
  const delta = (max - min) * config.deltaPercent;

  const lastPosition = obvs.length - 1;
  const isGoingUp =
    obvs[lastPosition] > obvs[lastPosition - 1] ||
    obvs[lastPosition] > obvs[lastPosition - 2];

  let idx = lastPosition;
  let tops = [];
  let bottoms = [];
  let obvValue = obvs[lastPosition];

  while (idx > 0) {
    let currentMax1 = lastPosition,
      currentMax2 = lastPosition,
      currentMin1 = lastPosition,
      currentMin2 = lastPosition;

    if (isGoingUp) {
      // 1st Min
      idx = minimun(idx, 0, obvValue, delta, obvs);
      currentMin1 = idx;
      obvValue = obvValue[idx];
      idx--;

      // 1st Max
      idx = maximun(idx, 0, obvValue, delta, obvs);
      currentMax1 = idx;
      obvValue = obvs[idx];
      idx--;
      // 2sd Min
      currentMin2 = minimun(idx, 0, obvValue, delta, obvs);

      const lastMinFound = check_min(currentMin1, currentMax1, obvs);
      const lastMaxFound = check_max(currentMax1, currentMin2, obvs);
      if (lastMinFound !== 0) bottoms.push(lastMinFound);
      if (lastMaxFound !== 0) tops.push(lastMaxFound);
    } else {
      // 1st Max
      idx = maximun(idx, 0, obvValue, delta, obvs);
      currentMax1 = idx;
      obvValue = obvs[idx];
      idx--;

      // 1st Min
      idx = minimun(idx, 0, obvValue, delta, obvs);
      currentMin1 = idx;
      obvValue = obvs[idx];
      idx--;
      // 2sd Max
      currentMax2 = maximun(idx, 0, obvValue, delta, obvs);

      const lastMaxFound = check_max(currentMax1, currentMin1, obvs);
      const lastMinFound = check_min(currentMin1, currentMax2, obvs);
      if (lastMaxFound !== 0) tops.push(lastMaxFound);
      if (lastMinFound !== 0) bottoms.push(lastMinFound);
    }
  }

  const offset = candles['close'].length - obvs.length - 1;

  return {
    high: tops.map((i) => ({
      obv: obvs[i],
      closeTime: candles['closeTime'][offset + i],
      close: candles['close'][offset + i],
    })),
    low: bottoms.map((i) => ({
      obv: obvs[i],
      closeTime: candles['closeTime'][offset + i],
      close: candles['close'][offset + i],
    })),
    obv: obvs,
  };
}

module.exports = obv;
