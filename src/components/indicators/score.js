const { ScoreGroup } = require('@prisma/client');

function calcPartialScore(analyze, group, flags) {
  return analyze.reduce((acc, cur) => {
    if (!cur) return acc;
    const flag = flags.filter((v) => v.group === group && v.tag === cur)[0];
    if (!flag) return acc;
    return (acc += flag.value);
  }, 0);
}

function calcScore(currencyResume, flags) {
  // get groups of flags.
  const groups = Object.keys(ScoreGroup);

  let generalScore = 1000; // initial score

  // calc score from indicadors.
  groups.forEach((group) => {
    // get indicators of the technical analysis
    let indicator = currencyResume.indicators[group];
    let partialScore = 0;
    if (indicator) {
      partialScore = calcPartialScore(indicator.analyze, group, flags);
      // partialScore = indicator.analyze.reduce((acc, cur) => {
      //   const flag = flags.filter((v) => v.group === group && v.tag === cur)[0];
      //   if (!flag) return acc;
      //   return (acc += flag.value);
      // }, 0);
      generalScore += partialScore;
      currencyResume.indicators[group] = { ...indicator, partialScore };
    }
  });

  // calc score from tendencies
  // EMA25
  const { tendencies25, tendencies7 } = currencyResume;
  let mediaTendency25 = 0;
  let priceTendency25 = 0;
  let mediaTendency7 = 0;
  let priceTendency7 = 0;

  // EMA25
  mediaTendency25 = calcPartialScore(
    tendencies25.mediaTendencies,
    'MEDIA_TENDENCIES',
    flags
  );
  priceTendency25 = calcPartialScore(
    tendencies25.priceTendencies,
    'PRICE_EMA_TENDENCIES_25',
    flags
  );

  let tendencyPartialScore = mediaTendency25 + priceTendency25;
  generalScore += tendencyPartialScore;

  currencyResume.tendencies25.result = {
    ...currencyResume.tendencies25.result,
    mediaScore: mediaTendency25,
    priceScore: priceTendency25,
    totalScore: tendencyPartialScore,
  };

  // EMA7
  mediaTendency7 = calcPartialScore(
    tendencies7.mediaTendencies,
    'MEDIA_TENDENCIES',
    flags
  );
  priceTendency7 = calcPartialScore(
    tendencies7.priceTendencies,
    'PRICE_EMA_TENDENCIES_7',
    flags
  );

  tendencyPartialScore = mediaTendency7 + priceTendency7;
  generalScore += tendencyPartialScore;

  currencyResume.tendencies7.result = {
    ...currencyResume.tendencies7.result,
    mediaScore: mediaTendency7,
    priceScore: priceTendency7,
    totalScore: tendencyPartialScore,
  };

  return {
    ...currencyResume,
    score: generalScore,
  };
}

module.exports = calcScore;
