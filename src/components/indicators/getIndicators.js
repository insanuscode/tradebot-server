const {
  SMA,
  bearishengulfingpattern,
  bullishengulfingpattern,
  darkcloudcover,
  doji,
  bearishharamicross,
  bullishharamicross,
  bullishmarubozu,
  bearishmarubozu,
  eveningdojistar,
  eveningstar,
  bearishharami,
  bullishharami,
  piercingline,
  morningstar,
} = require('technicalindicators');
const getCandles = require('../../util/getCandles');
const getExchangeInfo = require('../../util/getExchangeInfo');
const bb = require('./bb');
const mediaMovelExponencial = require('./ema');
const Extremes = require('./extremes');
const fibonacci = require('./fibonacci');
const macd = require('./macd');
const rsi = require('./RSI');
const stochastic = require('./stochastic');
const getTendencies = require('./tendencies');
const { ScoreGroup } = require('@prisma/client');

module.exports = async (iaConfig, indicatorsConfig, currencySymbols) => {
  try {
    if (!iaConfig) throw new Error('Ia configuration is empty');
    if (!indicatorsConfig)
      throw new Error('Indicadotors configuration is empty');
    if (!currencySymbols || currencySymbols.length === 0)
      throw new Error('No currency pair was specifield');

    // Get exchange info for the currencies at the currencySymbols.
    let exchangeInfo = await getExchangeInfo(currencySymbols);

    const currency = currencySymbols.map(async (currencyPair) => {
      // Get candles information for a spesific currency pair
      const candlesTime1 = await getCandles(
        currencyPair,
        '4h',
        indicatorsConfig.candlesNumber
      );

      // const candlesTime2 = await getCandles(
      //   currencyPair,
      //   '4h',
      //   indicatorsConfig.candlesNumber
      // );

      // Get a currency info for the 'currencyPair'
      const currencyInfo = exchangeInfo.find(
        (info) => info.symbol === currencyPair
      );

      // Calc and return the extremes sorted from the oldest to newest
      // Current candles are not taking in account
      if (
        ['UNIUSDT', 'LINKUSDT', 'BNBUSDT', 'ICPUSDT'].includes(currencyPair)
      ) {
        console.log('Uni');
      }
      let extremesTime1 = Extremes.getExtremes(candlesTime1, currencyInfo);
      // let extremesTime2 = Extremes.getExtremes(candlesTime2, currencyInfo);

      // the extremes are from the older to the newer.
      // so get the newer extreme founded
      if (extremesTime1.length <= 0) {
        // || extremesTime2.length <= 0
        // return currencyInfo;
        throw new Error('Cannot found extremes');
      }
      if (extremesTime1.length === 1) {
        console.log('M');
      }
      const firstExtreme1 = extremesTime1[extremesTime1.length - 1];
      // const firstExtreme2 = extremesTime2[extremesTime2.length - 1];

      // look for it's position on candles array
      const firstExtremePosition1 = candlesTime1['openTime'].indexOf(
        firstExtreme1.openTime
      );
      // const firstExtremePosition2 = candlesTime2['openTime'].indexOf(
      //   firstExtreme2.openTime
      // );

      if (firstExtremePosition1 === -1) {
        // && firstExtremePosition2
        console.log(findFirstExtreme);
        throw new Error(
          "None extreme was found so that we can't search for a corresponding candle."
        );
      }

      const extreme1 = {
        ...firstExtreme1,
        position: firstExtremePosition1,
      };
      // const extreme2 = {
      //   ...firstExtreme2,
      //   position: firstExtremePosition2,
      // };

      const mediaExp25 = mediaMovelExponencial.calc(candlesTime1, 25);
      const mediaExp7 = mediaMovelExponencial.calc(candlesTime1, 7);

      console.log('Currency', currencyPair);
      const extremesTendencies = Extremes.analyzeTendencies(extremesTime1);
      // const extremesTendencies = 'test';
      return {
        symbol: currencyPair,
        config: indicatorsConfig,
        indicators: {
          [ScoreGroup.RSI]: rsi(candlesTime1, indicatorsConfig, extremesTime1),
          [ScoreGroup.MACD]: macd(candlesTime1, indicatorsConfig),
          [ScoreGroup.BB]: bb(candlesTime1, indicatorsConfig),
          sma: sma(candlesTime1, indicatorsConfig),
          [ScoreGroup.STOCHASTIC]: stochastic(candlesTime1, indicatorsConfig),
          // obv: obv(candles),
          [ScoreGroup.EMA]: mediaMovelExponencial.analyze(candlesTime1),
        },
        candleSticksPatters: {
          bearishEngulfing: bearishEngulfing(candlesTime1, extreme1),
          bullishEngulfing: bullishEngulfing(candlesTime1, extreme1),
          darkcloud: darkcloud(candlesTime1, extreme1),
          doji: dojiPattern(extreme1),
          bearishHaramiCross: bearishHaramiCross(candlesTime1, extreme1),
          bullishHaramiCross: bullishHaramiCross(candlesTime1, extreme1),
          bullishMorubozu: bullishMorubozu(candlesTime1, extreme1),
          bearishMorubozu: bearishMorubozu(candlesTime1, extreme1),
          eveningDojiStar: eveningDojiStar(candlesTime1, extreme1),
          eveningStar: eveningStar(candlesTime1, extreme1),
          bearishHarami: bearishHarami(candlesTime1, extreme1),
          bullishHarami: bullishHarami(candlesTime1, extreme1),
          pierchingLine: pierchingLine(candlesTime1, extreme1),
          morningStar: morningStar(candlesTime1, extreme1),
        },
        extremes: extremesTime1,
        extremesTendencies,
        tendencies25: getTendencies(candlesTime1, mediaExp25),
        tendencies7: getTendencies(candlesTime1, mediaExp7),
        fibo: fibonacci(candlesTime1, extremesTime1),
      };
    });

    return Promise.all(currency);
  } catch (e) {
    console.log(e);
  }
};

// Simple Moving Average (SMA).
function sma(candles, config) {
  const { smaPeriod } = config;
  const input = {
    period: smaPeriod,
    values: candles['close'],
  };

  return SMA.calculate(input);
}

/**
 *
 * Analises graficas
 *
 */

// Bearish Engulfing Pattern
function bearishEngulfing(candles, extreme) {
  if (extreme.type === 'bottom') return false;
  if (extreme.position === 0 || extreme.position === candles['open'].length - 1)
    return false;

  const first = extreme.position - 1;
  const second = extreme.position;

  const input = {
    open: [candles['open'][first], candles['open'][second]],
    high: [candles['high'][first], candles['high'][second]],
    close: [candles['close'][first], candles['close'][second]],
    low: [candles['low'][first], candles['low'][second]],
  };

  return bearishengulfingpattern(input);
}

// Bullish Engulfing Pattern
function bullishEngulfing(candles, extreme) {
  if (extreme.type === 'top') return false;
  if (extreme.position === 0) return false;

  const first = extreme.position - 1;
  const second = extreme.position;

  const input = {
    open: [candles['open'][first], candles['open'][second]],
    high: [candles['high'][first], candles['high'][second]],
    close: [candles['close'][first], candles['close'][second]],
    low: [candles['low'][first], candles['low'][second]],
  };

  return bullishengulfingpattern(input);
}

// Dark Cloud Cover
function darkcloud(candles, extreme) {
  if (extreme.type === 'bottom') return false;
  if (extreme.position === 0) return false;

  const first = extreme.position - 1;
  const second = extreme.position;

  const input = {
    open: [candles['open'][first], candles['open'][second]],
    high: [candles['high'][first], candles['high'][second]],
    close: [candles['close'][first], candles['close'][second]],
    low: [candles['low'][first], candles['low'][second]],
  };

  return darkcloudcover(input);
}

// Doji
function dojiPattern(extreme) {
  const input = {
    open: [extreme.open],
    high: [extreme.high],
    close: [extreme.close],
    low: [extreme.low],
  };

  return doji(input);
}

// Bearish Harami Cross
function bearishHaramiCross(candles, extreme) {
  if (extreme.type === 'bottom') return false;
  const { position } = extreme;
  if (position === 0 || position === candles['open'].length - 1) return false;

  const first = position;
  const second = position + 1;

  const input = {
    open: [candles['open'][first], candles['open'][second]],
    high: [candles['high'][first], candles['high'][second]],
    close: [candles['close'][first], candles['high'][second]],
    low: [candles['low'][first], candles['low'][second]],
  };

  return bearishharamicross(input);
}

// Bullish Harami Cross
function bullishHaramiCross(candles, extreme) {
  if (extreme.type === 'top') return false;
  const { position } = extreme;
  if (position === 0 || position === candles['open'].length - 1) return false;

  const first = position;
  const second = position + 1;
  const input = {
    open: [candles['open'][first], candles['open'][second]],
    high: [candles['high'][first], candles['high'][second]],
    close: [candles['close'][first], candles['close'][second]],
    low: [candles['low'][first], candles['low'][second]],
  };

  return bullishharamicross(input);
}

// Bullish Marubozu (Candle Elefante alta)
function bullishMorubozu(candles, extreme) {
  if (extreme.type === 'top') return false;

  const { position } = extreme;
  const input = {
    open: [candles['open'][position]],
    high: [candles['high'][position]],
    close: [candles['close'][position]],
    low: [candles['low'][position]],
  };

  return bullishmarubozu(input);
}

// Bearish Marubozu (Candle Elefante baixa)
function bearishMorubozu(candles, extreme) {
  if (extreme.type === 'bottom') return false;

  const { position } = extreme;
  const first = position;

  const input = {
    open: [candles['open'][first]],
    high: [candles['high'][first]],
    close: [candles['close'][first]],
    low: [candles['low'][first]],
  };

  return bearishmarubozu(input);
}

// Evening Doji Star
function eveningDojiStar(candles, extreme) {
  if (extreme.type === 'bottom') return false;

  const { position } = extreme;
  if (position === 0 || position === candles['open'].length - 1) return false;
  const first = position - 1;
  const second = position;
  const third = position + 1;

  const input = {
    open: [
      candles['open'][first],
      candles['open'][second],
      candles['open'][third],
    ],
    high: [
      candles['high'][first],
      candles['high'][second],
      candles['high'][third],
    ],
    close: [
      candles['close'][first],
      candles['close'][second],
      candles['close'][third],
    ],
    low: [candles['low'][first], candles['low'][second], candles['low'][third]],
  };

  return eveningdojistar(input);
}

// Evening Star
function eveningStar(candles, extreme) {
  if (extreme.type === 'bottom') return false;
  const { position } = extreme;
  if (position === 0 || position === candles['open'].length - 1) return false;

  const first = position - 1;
  const second = position;
  const third = position + 1;

  const input = {
    open: [
      candles['open'][first],
      candles['open'][second],
      candles['open'][third],
    ],
    high: [
      candles['high'][first],
      candles['high'][second],
      candles['high'][third],
    ],
    close: [
      candles['close'][first],
      candles['close'][second],
      candles['close'][third],
    ],
    low: [candles['low'][first], candles['low'][second], candles['low'][third]],
  };

  return eveningstar(input);
}

// Bearish Harami
function bearishHarami(candles, extreme) {
  if (extreme.type === 'bottom') return false;
  const { position } = extreme;
  if (position === 0 || position === candles['open'].length - 1) return false;

  const first = position;
  const second = position + 1;

  const input = {
    open: [candles['open'][first], candles['open'][second]],
    high: [candles['high'][first], candles['high'][second]],
    close: [candles['close'][first], candles['close'][second]],
    low: [candles['low'][first], candles['low'][second]],
  };

  return bearishharami(input);
}

// Bullish Harami
function bullishHarami(candles, extreme) {
  if (extreme.type === 'top') return false;
  const { position } = extreme;
  if (position === 0 || position === candles['open'].length - 1) return false;

  const first = position;
  const second = position + 1;

  const input = {
    open: [candles['open'][first], candles['open'][second]],
    high: [candles['high'][first], candles['high'][second]],
    close: [candles['close'][first], candles['close'][second]],
    low: [candles['low'][first], candles['low'][second]],
  };

  return bullishharami(input);
}

// Piercing Line
function pierchingLine(candles, extreme) {
  if (extreme.type === 'top') return false;

  const { position } = extreme;
  const first = position - 1;
  const second = position;

  const input = {
    open: [candles['open'][first], candles['open'][second]],
    high: [candles['high'][first], candles['high'][second]],
    close: [candles['close'][first], candles['close'][second]],
    low: [candles['low'][first], candles['low'][second]],
  };

  return piercingline(input);
}

// Morning Star
function morningStar(candles, extreme) {
  if (extreme.type === 'top') return false;
  const { position } = extreme;
  if (position === 0 || position === candles['open'].length - 1) return false;

  const first = position - 1;
  const second = position;
  const third = position + 1;

  const input = {
    open: [
      candles['open'][first],
      candles['open'][second],
      candles['open'][third],
    ],
    high: [
      candles['high'][first],
      candles['high'][second],
      candles['high'][third],
    ],
    close: [
      candles['close'][first],
      candles['close'][second],
      candles['close'][third],
    ],
    low: [
      candles['low'][first],
      candles['open'][second],
      candles['open'][third],
    ],
  };

  return morningstar(input);
}

/**
 *
 * Top e fundo
 *
 */

function findFirstExtreme(candles) {
  function Point() {
    return 0.00001;
  }
  // Na binance os valores são da ordem de 0,00000001
  // Normalmente um pip é 0,0001, exerto pra yen que é 0,01
  // 1 pip = 10 pontos
  // 1 ponto é a menor unidade de variação, logo podemos afirmar que na binance um ponto é 0,00000001 e um pip é 0,00000010

  const bars = candles.close.length; //  Pesquisando a variação do ponto extremo
  const delta_points = 40; //  160 - Faixa de variação que define a distância mínima entre um pico e um fundo em pontos
  const first_extrem = 0.95; // 0.9 - Proporção adicional para procurar o primeiro valor extremo
  const reload_time = 5; //  Intervalo de tempo, após o qual os valores dos indicadores são recalculados, em segundos

  const candlesCopy = { ...candles };

  // Our candles are in the release order, so it starts by the oldest and finish with the newest.
  // But the extremes needs the reverse order.
  // Here we reverse each array that composes candles. like close.reverse, high.reverse, open.reverse and so on.
  const cand = Object.keys(candlesCopy).reduce((prev, key) => {
    const temp = [...candlesCopy[key]];
    candlesCopy[key] = temp.reverse().slice(1);
    prev[key] = candlesCopy[key];
    return prev;
  }, {});

  let { high, low, openTime, close } = cand;

  // let high = cand.high;
  // let low = cand.low;
  // let openTime = cand.openTime;
  // let close = cand.close;

  let delta = delta_points * Point(); //  Variação entre pontos extremos em termos absolutos

  let j, k, l;
  let j2, k2, l2;
  let j1, k1, l1;
  let min = []; // array que define fundos, o valor corresponde ao índice de barras para um ponto extremo detectado
  let max = []; // array que define os picos, o valor corresponde ao índice de barras para um ponto extremo detectado

  let mag1 = bars;
  let mag2 = bars;
  let mag3 = bars;
  let mag4 = bars;

  // close[0] deve ser substiduido pelo best bid pergar no Symbol book ticker => /api/v3/ticker/bookTicker
  j1 = close[1] + (1 - first_extrem) * delta;
  // Quando se procura o primeiro ponto extremo, a proporção adicional define o preço mínimo, abaixo do qual se situará o primeiro fundo

  j2 = 0; // Na primeira iteração, a busca é realizada a partir da última barra do histórico

  for (
    j = 0;
    j <= 15;
    j++ // loop que define o primeiro fundo - min [1]
  ) {
    min[1] = minimum(j2, bars, j1);
    //define o fundo mais próximo dentro do intervalo especificado

    j2 = min[1] + 1; // Na próxima iteração, a busca é realizada a partir do fundo min[1] detectado
    j1 = low[min[1]] + delta;
    //A Mínima do preço para o fundo detectado na iteração subseqüente deve ser menor que o preço baixo para o fundo encontrado na iteração atual

    k1 = low[min[1]];
    //Mínimo do preço para a parte inferior quando se procura o próximo ponto extremo define a Máxima do preço, acima do qual o pico deve ser localizado

    k2 = min[1]; //a procura do pico localizado após o fundo é realizado a partir do fundo min[1] detectado

    for (
      k = 0;
      k <= 12;
      k++ // Loop que define o primeiro pico - max[1]
    ) {
      max[1] = maximum(k2, bars, k1);
      //--- Definir o pico mais próximo num intervalo especificado
      k1 = high[max[1]] - delta;
      //A Máxima do preço para a próxima iteração deve exceder a Máxima para o pico detectado na iteração atual

      k2 = max[1] + 1; // Na próxima iteração, a pesquisa é realizada a partir do pico já detectado max[1]

      l1 = high[max[1]];
      //A Máxima do preço para o ponto extremo ao procurar o próximo fundo define o Mínimo do preço, abaixo do qual o fundo deve ser localizado
      l2 = max[1]; // Procurar o fundo localizado após o pico é realizado a partir do pico máximo detectado [1]
      for (
        l = 0;
        l <= 10;
        l++ // Loop definindo o segundo fundo - min[2] e o segundo pico max[2]
      ) {
        min[2] = minimum(l2, bars, l1);
        //---Definir o fundo mais próximo dentro do intervalo especificado
        l1 = low[min[2]] + delta;
        // Mínimo do preço para o fundo detectado na iteração subseqüente deve ser menor que o Mínimo do preço para o fundo encontrado na iteração atual

        l2 = min[2] + 1; // Na próxima iteração, a busca é realizada a partir do fundo min[2] detectado
        max[2] = maximum(min[2], bars, low[min[2]]);
        //Definir o pico mais próximo num intervalo especificado

        if (
          max[1] > min[1] &&
          min[1] > 0 &&
          min[2] > max[1] &&
          min[2] < max[2] &&
          max[2] < mag4
        ) {
          // Classificar valores extremos coincidentes e casos especiais
          mag1 = min[1]; // Em cada iteração, os locais dos valores extremos detectados são salvos se a condição for atendida
          mag2 = max[1];
          mag3 = min[2];
          mag4 = max[2];
        }
      }
    }
  }
  min[1] = mag1; // Pontos extremos são definidos, caso contrário o valor 'barras' é atribuído a todas as variáveis
  max[1] = mag2;
  min[2] = mag3;
  max[2] = mag4;

  function minimum(a, b, price0, candles) {
    //a função define o fundo mais próximo no intervalo especificado. O fundo está localizado abaixo do preço 0 a uma distância maior do que a faixa de variação
    // let { high, low } = candles;
    // high = [...high].reverse();
    // low = [...low].reverse();

    let i,
      e = b;

    let pr = price0 - delta; // O preço abaixo do qual o fundo com a variação adicionada deve ser localizado
    for (
      i = a;
      i <= b;
      i++ // Procure o fundo dentro do intervalo especificado pelos parâmetros a e b
    ) {
      if (low[i] < pr && low[i] < low[i + 1]) {
        // Definir o fundo mais próximo, após o crescimento dos preços começa
        e = i;
        break;
      }
    }

    return e;
  }

  function maximum(a, b, price1, candles) {
    //--- A função define o pico mais próximo no intervalo especificado. O fundo está localizado acima do price1 a uma distância superior à faixa de variação
    // let { high, low } = candles;
    // high = [...high].reverse();
    // low = [...low].reverse();

    let i,
      e = bars;

    let pr1 = price1 + delta; // O preço acima do qual o pico com a variação adicionada deve ser localizado
    for (
      i = a;
      i <= b;
      i++ // Procure o pico dentro do intervalo especificado pelos parâmetros a e b
    ) {
      if (high[i] > pr1 && high[i] > high[i + 1]) {
        // Definir o pico mais próximo, após o preço começar a cair
        e = i;
        break;
      }
    }
    return e;
  }

  // a função para verificar e corrigir a posição inferior dentro do intervalo especificado
  function check_min(a, b) {
    //  double high[],low[];
    //  int copied6=Copylow(Symbol(),0,0,bars+1,low);
    let i,
      c = a;
    for (
      i = a + 1;
      i < b;
      i++ // Ao procurar o fundo, todas as barras especificadas pelo intervalo são verificadas
    ) {
      if (low[i] < low[a] && low[i] < low[c])
        // Se o fundo localizado mais baixo for encontrado
        c = i; // O local do fundo é redefinido
    }
    return c;
  }

  //--- A função para verificar e corrigir a posição de pico dentro do intervalo especificado
  function check_max(a, b) {
    //  double high[],low[];
    //  ArraySetAsSeries(high,true);
    //  int copied7=Copyhigh(Symbol(),0,0,bars+1,high);
    let i,
      d = a;

    for (
      i = a + 1;
      i < b;
      i++ // Ao procurar o fundo, todas as barras especificadas pelo intervalo são verificadas
    ) {
      if (high[i] > high[a] && high[i] > high[d])
        // Se o pico localizado mais alto for encontrado
        d = i; // A localização do pico é redefinida
    }
    return d;
  }

  let min1 = check_min(min[1], max[1]); // Verificar e corrigir a posição do primeiro fundo dentro do intervalo especificado
  let max1 = check_max(max[1], min[2]); // Verificar e corrigir a posição do primeiro pico dentro do intervalo especificado
  let min2 = check_min(min[2], max[2]); // Verificar e corrigir a posição do segundo fundo dentro do intervalo especificado

  return {
    min: [
      {
        close: cand.close[min1],
        time: cand.closeTime[min1],
      },
      {
        close: cand.close[min2],
        time: cand.closeTime[min2],
      },
    ],
    max: [{ close: cand.close[max1], time: cand.closeTime[max1] }],
  };
}
