function fibonacci(candles, extremes) {
  // last position is the newest extreme.
  const lastPosition = extremes.length - 1;
  const extreme1 = extremes[lastPosition - 1]; // second newest extreme
  const extreme2 = extremes[lastPosition]; // newest extreme

  // Verify if the newest extreme are in the top or in the bottom.
  const isFirstTop = extreme2.type.toLowerCase() === 'top';

  // Get last candle(newest).
  const lastCandle = candles['close'].length - 1;
  const price = candles['close'][lastCandle];

  let pr2 = 0; // newest extreme price.
  let pr1 = 0; // second newest extreme price.

  let isBetween = false; // Flag to verify if the price is between the top and bottom.

  if (isFirstTop) {
    // if the newest extreme is a top. pr2 get the highest and pr1 get the lowest.
    if (extreme1 === undefined || extreme2 === undefined)
      console.log('they are undefined');
    pr2 = Math.max(extreme2.close, extreme2.open);
    pr1 = Math.min(extreme1.close, extreme1.open);
    isBetween = price <= pr2 && price >= pr1;
  } else {
    // if the first extreme is a bottom. pr2 get the lowest and pr1 get the highest.
    if (extreme1 === undefined || extreme2 === undefined)
      console.log('they are undefined');
    pr2 = Math.min(extreme2.close, extreme2.open);
    pr1 = Math.max(extreme1.close, extreme1.open);
    isBetween = price >= pr2 && price <= pr1;
  }

  const deltaExtreme = Math.abs(pr2 - pr1); // difference between top and bottom;
  let deltaPrice = 0; // difference between last price and one of the extreme(top or bottom).
  let place; // Flag for price position. BETWEEN | OVER | UNDER

  if (isBetween) {
    place = 'BETWEEN';
    deltaPrice = isFirstTop ? pr2 - price : price - pr2;
  } else {
    const max = Math.max(pr2, pr1);
    const min = Math.min(pr2, pr1);

    if (price > max) {
      place = 'OVER';
      deltaPrice = price - min;
    } else if (price < min) {
      place = 'UNDER';
      deltaPrice = max - price;
    }
  }

  // percent relation
  let percent = (deltaPrice / deltaExtreme) * 100;
  if (place === 'UNDER') percent *= -1;

  let level = 0;
  if (percent < 0) {
    level = -99;
  } else if (percent <= 23.6) {
    level = 1;
  } else if (percent <= 38.2) {
    level = 2;
  } else if (percent <= 50) {
    level = 3;
  } else if (percent <= 61.8) {
    level = 4;
  } else if (percent <= 100) {
    level = 5;
  } else {
    level = 99;
  }

  // Todo level
  let nextExtreme;
  if (level > 0 && level <= 5) {
    nextExtreme = extreme1;
  } else if (level < 0) {
    nextExtreme = extremes
      .concat()
      .reverse()
      .find(
        (v) =>
          v.type.toLowerCase() === 'bottom' &&
          (v.close < price || v.open < v.open < price)
      );
  } else if (level > 90) {
    nextExtreme = extremes
      .concat()
      .reverse()
      .find(
        (v) =>
          v.type.toLowerCase() === 'top' && (v.close > price || v.open > price)
      );
  }

  return {
    level,
    percent,
    place,
    first: isFirstTop ? 'TOP' : 'BOTTOM',
    nextExtreme,
    price,
    deltaPrice,
    deltaExtreme,
    top: isFirstTop ? extreme2 : extreme1,
    bottom: isFirstTop ? extreme1 : extreme2,
    candles,
    extremes,
  };
}

module.exports = fibonacci;
