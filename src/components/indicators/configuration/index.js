const router = require('express').Router();
const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient();

router.get('/', async (req, res) => {
  try {
    const config = await prisma.indicatorsConfig.findFirst();
    res.json(config);
  } catch (e) {
    console.log(e);
    res.status(400);
  }
});

router.patch('/', async (req, res) => {
  try {
    const {
      candlesNumber,
      rsiperiod,
      macdFastPeriod,
      macdSlowPeriod,
      macdSignalPeriod,
      macdSimpleMAOscillator,
      macdSimpleMASignal,
      bbPeriod,
      bbStdDev,
      smaPeriod,
      stochasticPeriod,
      stochasticSignalPeriod,
    } = req.body;

    console.log('Dado original', req.body);
    const data = {
      candlesNumber: Number(candlesNumber),
      rsiperiod: Number(rsiperiod),
      macdFastPeriod: Number(macdFastPeriod),
      macdSlowPeriod: Number(macdSlowPeriod),
      macdSignalPeriod: Number(macdSignalPeriod),
      macdSimpleMAOscillator,
      macdSimpleMASignal,
      bbPeriod: Number(bbPeriod),
      bbStdDev: Number(bbStdDev),
      smaPeriod: Number(smaPeriod),
      stochasticPeriod: Number(stochasticPeriod),
      stochasticSignalPeriod: Number(stochasticSignalPeriod),
    };

    console.log('This will be persisted', data);
    const result = await prisma.indicatorsConfig.update({
      data,
      where: {
        id: 1,
      },
    });
    res.json(result);
  } catch (e) {
    console.log(e);
    res.status(400);
  }
});

module.exports = (app) => {
  app.use('/configuration', router);
};
