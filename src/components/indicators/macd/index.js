const { MACD } = require('technicalindicators');
const { ScoreGroup } = require('@prisma/client');

function calc(candles, config) {
  const {
    macdFastPeriod,
    macdSlowPeriod,
    macdSignalPeriod,
    macdSimpleMAOscillator,
    macdSimpleMASignal,
  } = config;

  const input = {
    values: candles['close'],
    fastPeriod: macdFastPeriod,
    slowPeriod: macdSlowPeriod,
    signalPeriod: macdSignalPeriod,
    SimpleMAOscillator: macdSimpleMAOscillator,
    SimpleMASignal: macdSimpleMASignal,
  };

  return MACD.calculate(input);
}

function analysis(macds) {
  const analize = macds.map((v, i, arr) => {
    if (i < 3) return null;

    const { MACD: macd2, signal: signal2 } = v;
    const { MACD: macd1, signal: signal1 } = arr[i - 1];
    // const { MACD: macd, signal } = arr[i - 2];

    // Cross zero down
    if (macd1 >= 0 && macd2 < 0) return 'CROSS_ZERO_DOWN';

    // Cross zero up
    if (macd1 <= 0 && macd2 > 0) return 'CROSS_ZERO_UP';

    // macd cross down sinal.
    if (macd1 >= signal1 && macd2 < signal2) return 'SELL';

    // macd cross up sinal.
    if (macd1 <= signal1 && macd2 > signal2) return 'BUY';

    return null;
  });

  return analize;
}

// Moving Average Convergence Divergence (MACD).
function macd(candles, config) {
  const result = calc(candles, config);
  const analize = analysis(result);
  return {
    [ScoreGroup.MACD]: result,
    analyze: analize,
  };
}

module.exports = macd;
