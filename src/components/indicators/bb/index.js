const { BollingerBands } = require('technicalindicators');
const { ScoreGroup } = require('@prisma/client');

function calc(candles, config) {
  const { bbPeriod, bbStdDev } = config;
  const input = {
    period: bbPeriod,
    values: candles['close'],
    stdDev: bbStdDev,
  };

  return BollingerBands.calculate(input);
}

function bb(candles, config) {
  const bbs = calc(candles, config);

  const offset = candles['close'].length - bbs.length;
  const result = bbs.map((v, i, arr) => {
    const { middle, upper, lower } = v;

    if (i < 1) return null;

    const idx = offset + i;
    const p1 = candles['close'][idx - 1];
    const p2 = candles['close'][idx];

    if (p1 > p2) {
      // cross down the upper line.
      if (p1 >= upper && p2 < upper) return 'MEDIUM_BEARISH_POSSILITY';
      // cross down the middle line.
      if (p1 >= middle && p2 < middle) return 'HIGH_BEARISH_POSSIBILITY';
      // cross down the low line.
      if (p1 >= lower && p2 < lower) return 'MEDIUM_BULLISH_POSSIBILITY';
    }

    if (p1 < p2) {
      // cross up the upper line.
      if (p1 <= upper && p2 > upper) return 'MEDIUM_BULLISH_POSSIBILITY';
      // cross up middle line.
      if (p1 <= middle && p2 > middle) return 'HIGH_BULLISH_POSSIBILITY';
      // cross up lower line.
      if (p1 <= lower && p2 > lower) return 'MEDIUM_BULLISH_POSSIBILITY';
    }

    if (p2 >= upper) return 'NULL_UPPER';
    if (p2 < upper && p2 >= middle) return 'BULLISH_POSSIBILITY';
    if (p2 < middle && p2 >= lower) return 'BEARISH_POSSIBILITY';

    // p2 < lower
    return 'NULL_LOWER';
  });

  return {
    [ScoreGroup.BB]: bbs,
    analyze: result,
  };
}

module.exports = bb;
