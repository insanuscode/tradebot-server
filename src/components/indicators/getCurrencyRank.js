const { PrismaClient } = require('@prisma/client');
const getSymbols = require('../../util/getSymbols');

async function getCurrencyRank(iaConfig) {
  // MarketCap
  let currencyRank = await getSymbols(iaConfig ? iaConfig['rank'] : 20);
  // pairs = pairs.map((p) =>
  //   p.concat(iaConfig ? iaConfig['stableCoin'] : 'USD')
  // );

  // Left side coins fromm database
  const prisma = new PrismaClient();
  let validPairs = await prisma.meta.findUnique({
    where: { description: 'VALID_PAIRS' },
  });
  validPairs = JSON.parse(validPairs.value);

  if (!validPairs)
    throw new Error(
      'Não foi possível recuperar os dados das criptomoedas da Binance'
    );

  currencyRank = currencyRank
    .map((m) => m.concat(iaConfig.stableCoin))
    .filter((p) => validPairs.includes(p))
    .slice(0, iaConfig.rank);

  return currencyRank;
}

module.exports = getCurrencyRank;
