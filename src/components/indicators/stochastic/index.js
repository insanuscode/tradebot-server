const { Stochastic } = require('technicalindicators');
const { ScoreGroup } = require('@prisma/client');

const stochasticConfg = {
  stochasticTop: 80,
  stochasticBottom: 20,
  stochasticMiddle: 50,
};

// Stochastic Oscillator (KD).
function calc(candles, config) {
  // console.log('Stochastic config', config);
  const { stochasticSignalPeriod, stochasticPeriod } = config;
  const input = {
    high: candles['high'],
    low: candles['low'],
    close: candles['close'],
    period: stochasticPeriod,
    signalPeriod: stochasticSignalPeriod,
  };

  return Stochastic.calculate(input);
}

function analysis(stochastics) {
  const { stochasticBottom, stochasticMiddle, stochasticTop } = stochasticConfg;

  function isMiddleLow(value) {
    return value > stochasticBottom && value <= stochasticMiddle;
  }

  function isMiddleHigh(value) {
    return value > stochasticMiddle && value < stochasticTop;
  }

  function isKCrossUpD(arr, i) {
    // const { k: k1, d: d1 } = arr[i - 2];
    const { k: k1, d: d1 } = arr[i - 1];
    const { k, d } = arr[i];

    return k1 <= d1 && k > d;
  }

  function isKCrossDownD(arr, i) {
    // const { k: k1, d: d1 } = arr[i - 2];
    const { k: k1, d: d1 } = arr[i - 1];
    const { k, d } = arr[i];

    return k1 >= d1 && k < d;
  }

  return stochastics.map((v, i, arr) => {
    if (i < 4) return null;

    const { k, d } = v;

    // Between 50 and 80
    if (isMiddleHigh(k)) {
      //  && isMiddleHigh(d)
      if (isKCrossUpD(arr, i)) {
        return 'MIDDLE_HIGH_CROSS_UP';
      }
      if (isKCrossDownD(arr, i)) {
        return 'MIDDLE_HIGH_CROSS_DOWN';
      }
      return 'BULLISH_POSSIBILITY';
    }

    // Between 20 and 50
    if (isMiddleLow(k)) {
      // && isMiddleLow(d)
      if (isKCrossUpD(arr, i)) {
        return 'MIDDLE_LOW_CROSS_UP';
      }
      if (isKCrossDownD(arr, i)) {
        return 'MIDDLE_LOW_CROSS_DOWN';
      }
      return 'BEARISH_POSSIBILITY';
    }

    // Under 20 range
    if (k <= stochasticBottom || d <= stochasticBottom) {
      if (isKCrossUpD(arr, i)) {
        return 'BUY';
      }
      return 'CHEAP';
    }

    // Over 80 range
    if (k >= stochasticTop || d >= stochasticTop) {
      if (isKCrossDownD(arr, i)) {
        return 'SELL';
      }
      return 'EXPENSIVE';
    }
  });
}

function stochastic(candles, config) {
  const stochasticIncators = calc(candles, config);
  const analyze = analysis(stochasticIncators);

  return {
    [ScoreGroup.STOCHASTIC]: stochasticIncators,
    analyze,
  };
}

module.exports = stochastic;
