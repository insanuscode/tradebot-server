const { RSI } = require('technicalindicators');
const { ScoreGroup } = require('@prisma/client');

const analysisConfig = {
  rsiCheapValue: 30,
  rsiExpensiveValue: 70,
  rsiMiddleValue: 50,
};

function calc(candles, config) {
  // console.log('RSI config', config);
  const { rsiperiod } = config;
  const input = {
    values: candles['close'],
    period: rsiperiod,
  };

  return RSI.calculate(input);
}

function analysis(rsiValues) {
  const { rsiCheapValue, rsiExpensiveValue, rsiMiddleValue } = analysisConfig;

  return rsiValues.map((v, i, arr) => {
    // cross up the high line
    if (i > 0 && arr[i - 1] <= rsiExpensiveValue && v > rsiExpensiveValue)
      return 'BUY_OVER';

    // cross down the high line
    if (i > 0 && arr[i - 1] >= rsiExpensiveValue && v < rsiExpensiveValue)
      return 'MEDIUM_BEARISH_POSSILITY';

    // cross up the middle line
    if (i > 0 && arr[i - 1] <= rsiMiddleValue && v > rsiMiddleValue)
      return 'HIGH_BULLISH_POSSIBILITY';
    // cross down the middle line
    if (i > 0 && arr[i - 1] >= rsiMiddleValue && v < rsiMiddleValue)
      return 'HIGH_BEARISH_POSSIBILITY';

    // cross up the low line
    if (i > 0 && arr[i - 1] <= rsiCheapValue && v > rsiCheapValue)
      return 'CHEAP_LOW_RISK';
    // cross down the low line
    if (i > 0 && arr[i - 1] >= rsiCheapValue && v < rsiCheapValue)
      return 'SELL';

    if (v <= rsiCheapValue && i > 0 && v > v[i - 1]) return 'BUY_UNDER';
    if (v <= rsiCheapValue) return 'CHEAP';
    if (v > rsiCheapValue && v <= rsiMiddleValue) return 'BEARISH_POSSIBILITY';
    if (v > rsiMiddleValue && v <= rsiExpensiveValue)
      return 'BULLISH_POSSIBILITY';
    if (v > rsiExpensiveValue) return 'EXPENSIVE';
  });
}

// Relmative Strength Index (RSI)
function rsi(candles, config, extremes) {
  const rsiIndicators = calc(candles, config);
  const analyze = analysis(rsiIndicators);

  const newestExtreme = extremes[extremes.length - 1];
  const lastCandle = candles['close'].length - 1;
  return {
    [ScoreGroup.RSI]: rsiIndicators,
    analyze,
    tendency:
      newestExtreme.type.toLowerCase() === 'top' &&
      candles['close'][lastCandle] < newestExtreme.close
        ? 'DOWN'
        : 'UP',
  };
}

module.exports = rsi;
