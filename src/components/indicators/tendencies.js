module.exports = function getTendencies(candles, ema) {
  const close = candles['close'];
  const periodo = close.length - ema.length;

  // Media
  const mediaTendencies = ema.map((v, i, arr) => {
    if (i === 0) return null;

    // return Up or Down.
    if (arr[i - 1] <= v) return 'UP';

    return 'DOWN';
  });

  // Price
  const priceTendencies = ema.map((v, i, arr) => {
    const current = periodo + i;
    const prev = periodo + i - 1;

    if (i > 0) {
      // Price cross up media
      if (close[prev] <= arr[prev] && close[current] > v) return 'UP_FLAG1';

      // Price cross down media
      if (close[prev] >= arr[prev] && close[current] < v) return 'DOWN_FLAG1';
    }

    if (i > 2) {
      // Price cross up media with two confirmation
      // Take four candles and EMA.
      // Verify if we have a cross up between the fist and second price/EMA. Where price cross up EMA.
      if (close[prev - 2] <= arr[prev - 2] && close[prev - 1] > arr[prev - 1]) {
        // Once we have a cross up. Now verify if the last two price stays above EMA.
        if (close[prev] > arr[prev] && close[current] > arr[current]) {
          return 'UP_CONFIRMED';
        }
      }

      // Price cross down media with two confirmation
      // Take four candles and EMA.
      // Verify if we have a cross down between the fist and second price/EMA. Where price cross down EMA.
      if (close[prev - 2] >= arr[prev - 2] && close[prev - 1] < arr[prev - 1]) {
        // Once we have a cross down. Now verify if the last two price stays under EMA.
        if (close[prev] < arr[prev] && close[current] < arr[current]) {
          return 'DOWN_CONFIRMED';
        }
      }
    }

    if (close[current] >= v) return 'UP';
    if (close[current] <= v) return 'DOWN';
  });

  const revertFlags = priceTendencies.map((v, i, arr) => {
    if (i === 0) return 0;

    const anterior = arr[i - 1];
    if (anterior === v) return 0;

    return 1;
  });

  const lastIndex = mediaTendencies.length - 1;
  return {
    mediaTendencies,
    priceTendencies,
    revertFlags,
    result: {
      tendency: mediaTendencies[lastIndex],
      revertFlags: revertFlags[lastIndex],
    },
  };
};
