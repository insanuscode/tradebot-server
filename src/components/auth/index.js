const router = require('express').Router();
const { PrismaClient, Prisma } = require('@prisma/client');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const mailer = require('../../util/mailer');
const loginHide = require('../../util/hideSecureData');

const prisma = new PrismaClient();

function generateToken(params = {}) {
  return jwt.sign(params, process.env.TOKEN_SECRET, {
    expiresIn: '2h',
  });
}

router.post('/register', async (req, res) => {
  try {
    const hash = await bcrypt.hash(req.body.password, 10);
    const user = await prisma.login.create({
      data: { ...req.body, password: hash },
    });

    loginHide.call(user);
    return res.send({ user, token: generateToken({ id: user.id }) });
  } catch (err) {
    if (err instanceof Prisma.PrismaClientKnownRequestError) {
      if (err.code === 'P2002') {
        return res.status(400).send({ error: 'O Email informado já existe.' });
      }
    }

    console.log(err);
    return res.status(400).send({ error: 'Registration failed' });
  }
});

router.post('/authenticate', async (req, res) => {
  const { email, password } = req.body;

  const user = await prisma.login.findFirst({ where: { email } });
  let meta = await prisma.meta.findMany();

  if (!user) return res.status(400).json({ error: 'Usuário não encontrado.' });
  if (!user.isActivate)
    return res.status(401).json({ error: 'Usuário inativo.' });
  if (!(await bcrypt.compare(password, user.password)))
    return res.status(400).json({ error: 'Senha inválida.' });

  loginHide.call(user);

  meta = meta.map((m) => ({ ...m, value: JSON.parse(m.value) }));

  const token = generateToken({ id: user.id });
  return res.json({ user, token, meta });
});

router.post('/forgot_password', async (req, res) => {
  const { email } = req.body;
  try {
    const user = await prisma.login.findFirst({ where: { email } });
    if (!user)
      return res.status(400).json({ error: 'Usuário não encontrado.' });

    const token = crypto.randomBytes(32).toString('hex');
    const now = new Date();
    now.setHours(now.getHours() + 1);

    await prisma.login.update({
      where: { id: user.id },
      data: {
        passwordResetToken: token,
        passwordResetExpires: now,
      },
    });

    console.log('before send email');
    mailer.sendMail(
      {
        to: email,
        from: 'insanubrasil@gmail.com',
        subject: 'Redefinição de senha.',
        template: 'forgot_password',
        context: { token },
      },
      (err) => {
        if (err) {
          console.log(err);
          return res
            .status(400)
            .json({ error: 'Não foi possível enviar o email de confirmação.' });
        }

        return res.send();
      }
    );

    console.log('after sanding email');
  } catch (err) {
    res.status(400).json({ error: 'Algo deu errado, tente novamete.' });
  }
});

module.exports = (app) => {
  app.use('/auth', router);
};
