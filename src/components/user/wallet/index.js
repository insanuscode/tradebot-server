const route = require('express').Router();
const axios = require('axios').default;
const { PrismaClient } = require('@prisma/client');

const binanceGenerateSignature = require('../../../util/binanceGenerateSignature');
const hideSecureData = require('../../../util/hideSecureData');
const authMiddleware = require('../../../middlewares/auth');

const prisma = new PrismaClient();
const binanceAxios = axios.create({
  baseURL: process.env.BINANCE_HOST,
});

route.use(authMiddleware);
route.get('/', async (req, res) => {
  const { viewCoin } = req.query;
  console.log('Query', req.query);
  try {
    const { userId } = req;
    const user = await prisma.login.findFirst({ where: { id: userId } });

    const params = {
      timestamp: Date.now(),
    };
    const hash = binanceGenerateSignature(params, user.secureKey);
    const response = await binanceAxios.get('/api/v3/account', {
      params: {
        ...params,
        signature: hash,
      },
      headers: {
        'X-MBX-APIKEY': user.key,
      },
    });

    let h24 = await binanceAxios.get('/api/v3/ticker/24hr');
    // console.log('h24', h24.data);
    let prices = await binanceAxios.get('/api/v3/ticker/price');
    // console.log('Prices', prices.data);

    console.log('ViewCoin Params', viewCoin);
    let finalprice = response.data.balances
      .filter((b) => Number(b.free) > 0 || Number(b.locked) > 0)
      .map((curency) => {
        const historic24 = h24.data.find((h) => {
          const pair = viewCoin
            ? curency.asset + viewCoin
            : curency.asset + user.viewCoin;
          // console.log('h24', pair);
          return h.symbol === pair;
        });

        const price = prices.data.find((p) => {
          const pair = viewCoin
            ? curency.asset + viewCoin
            : curency.asset + user.viewCoin;
          return p.symbol === pair;
        });

        const wallet = curency;
        wallet['percent'] = historic24 ? historic24.priceChangePercent : 0;
        wallet['price'] = price ? price.price : 1;
        wallet['dollar'] =
          Number(wallet.price) * (Number(wallet.locked) + Number(wallet.free));

        return wallet;
      });

    // return res.json({ wallet: response.data, h24, prices });
    return res.json(finalprice);
  } catch (err) {
    console.log(err);
    if (err['code'])
      return res.status(401).json({
        error: 'Algo errado com as suas chaves Binance por favor redefinir.',
      });
    return res.status(400).json({ error: err.message });
  }
});

route.patch('/currency', async (req, res) => {
  try {
    const { userId } = req;
    const { currency } = req.body;
    console.log('Body ======>>> ', req.body);
    const user = await prisma.login.updateMany({
      data: { viewCoin: currency },
      where: { id: userId },
    });

    hideSecureData.call(user);
    return res.json(user);
  } catch (err) {
    console.log(err);
    return res
      .status(400)
      .json({ error: 'Não foi possível atualizar o valor de viewCoin' });
  }
});
module.exports = (app) => app.use('/user/wallet', route);
