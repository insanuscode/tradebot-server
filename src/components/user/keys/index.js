const route = require('express').Router();
const axios = require('axios').default;
const { PrismaClient } = require('@prisma/client');

const auth = require('../../../middlewares/auth');
const hideSecureData = require('../../../util/hideSecureData');
const buildSign = require('../../../util/binanceGenerateSignature');

const binanceAxios = axios.create({
  baseURL: process.env.BINANCE_HOST,
});

const prisma = new PrismaClient();

function validateKeysMiddleware(req, res, next) {
  const { key, secureKey } = req.body;
  try {
    if (!key || !secureKey) throw new EvalError('As chaves são obrigatórias');
    if (key === secureKey) throw new EvalError('Chaves não podem ser iguais.');
    if (typeof key !== 'string' || typeof secureKey !== 'string')
      throw new EvalError('As chaves devem ser uma sequência de caracteres.');
    if (key.length < 64 || secureKey.length < 64)
      throw new EvalError('As chaves deve ser uma sequência de 64 caracteres.');

    next();
  } catch (err) {
    // if (err.name === 'EvalError')
    return res.status(400).json({ error: err.message });
  }
}

route.use(auth);
route.get('/', async (req, res) => {
  const { userId } = req;

  const user = await prisma.login.findFirst({ where: { id: userId } });

  // henerate signature
  const timestamp = Date.now();
  const params = {
    timestamp,
  };
  const hash = buildSign(params, user.secureKey);

  const acountInfo = await binanceAxios.get('/api/v3/account', {
    params: {
      ...params,
      signature: hash,
    },
    headers: {
      'X-MBX-APIKEY': user.key,
    },
  });

  res.send(acountInfo.data);
});
route.patch('/', validateKeysMiddleware, async (req, res) => {
  const { key, secureKey } = req.body;

  try {
    const user = await prisma.login.update({
      data: { key, secureKey },
      where: { id: req.userId },
    });
    hideSecureData.call(user);

    return res.json(user);
  } catch (err) {
    return res.status(400).json({
      error: 'Algo deu errado. Verifique as informações e tente novamente.',
    });
  }
});

module.exports = (app) => app.use('/user/keys', route);
