const express = require('express');
const cors = require('cors');
const indicadores = require('./components/indicators/getIndicators');
require('dotenv').config();

const app = express();
app.use(cors());
app.use(express.json());

require('./components/ia')(app);
require('./components/indicators')(app);
require('./components/auth')(app);
require('./components/user/keys')(app);
require('./components/user/wallet')(app);

app.listen(5000, () => {
  console.log('Server is runing...\n\n');
});

// indicadores().then((r) => {
//   Promise.all(r).then((e) => {
//     console.log('Result => ', JSON.stringify(e, null, 2));
//   });
// });
