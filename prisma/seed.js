const { PrismaClient } = require('@prisma/client');
const indicatorsConfig = require('../src/util/seeds/IndicadorsConfig');
const scoreDefault = require('../src/util/seeds/scoreDefault');
const scoreConfigurations = require('../src/util/seeds/scoreDefault');

const prisma = new PrismaClient();

async function main() {
  // await prisma.indicatorsConfig.create({
  //   data: indicatorsConfig,
  // });
  await prisma.scoreConfigurations.createMany({
    data: scoreDefault,
  });
}

main()
  .catch((e) => {
    console.log(e);
    process.exit(1);
  })
  .finally(() => {
    prisma.$disconnect;
  });
