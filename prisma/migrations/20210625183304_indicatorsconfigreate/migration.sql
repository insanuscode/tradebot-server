-- CreateTable
CREATE TABLE "IndicatorsConfig" (
    "id" SERIAL NOT NULL,
    "candlesNumber" INTEGER NOT NULL,
    "rsiperiod" INTEGER NOT NULL,
    "macdFastPeriod" INTEGER NOT NULL,
    "macdSlowPeriod" INTEGER NOT NULL,
    "macdSignalPeriod" INTEGER NOT NULL,
    "macdSimpleMAOscillator" BOOLEAN NOT NULL,
    "macdSimpleMASignal" BOOLEAN NOT NULL,
    "bbPeriod" INTEGER NOT NULL,
    "bbStdDev" INTEGER NOT NULL,
    "smaPeriod" INTEGER NOT NULL,
    "stochasticPeriod" INTEGER NOT NULL,
    "stochasticSignalPeriod" INTEGER NOT NULL,

    PRIMARY KEY ("id")
);
