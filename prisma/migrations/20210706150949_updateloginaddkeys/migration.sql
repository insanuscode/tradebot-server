-- AlterTable
ALTER TABLE "Login" ADD COLUMN     "isKeysValid" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "key" TEXT,
ADD COLUMN     "secureKey" TEXT;
