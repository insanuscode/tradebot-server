/*
  Warnings:

  - Changed the type of `group` on the `ScoreConfigurations` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.

*/
-- AlterTable
ALTER TABLE "ScoreConfigurations" DROP COLUMN "group",
ADD COLUMN     "group" "ScoreGroup" NOT NULL;
