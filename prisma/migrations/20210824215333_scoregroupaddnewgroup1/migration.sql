/*
  Warnings:

  - The values [PRICE_EMA_TENDENCIES] on the enum `ScoreGroup` will be removed. If these variants are still used in the database, this will fail.

*/
-- AlterEnum
BEGIN;
CREATE TYPE "ScoreGroup_new" AS ENUM ('RSI', 'STOCHASTIC', 'MACD', 'EMA', 'BB', 'MEDIA_TENDENCIES', 'PRICE_EMA_TENDENCIES_25', 'PRICE_EMA_TENDENCIES_7', 'FIBONACCI');
ALTER TABLE "ScoreConfigurations" ALTER COLUMN "group" TYPE "ScoreGroup_new" USING ("group"::text::"ScoreGroup_new");
ALTER TYPE "ScoreGroup" RENAME TO "ScoreGroup_old";
ALTER TYPE "ScoreGroup_new" RENAME TO "ScoreGroup";
DROP TYPE "ScoreGroup_old";
COMMIT;
