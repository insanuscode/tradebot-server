-- CreateEnum
CREATE TYPE "ScoreGroup" AS ENUM ('RSI', 'STOCHASTIC', 'MACD', 'EMA', 'BB', 'MEDIA_TENDENCIES', 'PRICE_EMA_TENDENCIES', 'FIBONACCI');

-- CreateTable
CREATE TABLE "ScoreConfigurations" (
    "id" SERIAL NOT NULL,
    "tag" TEXT NOT NULL,
    "value" INTEGER NOT NULL,
    "description" TEXT NOT NULL,
    "group" TEXT NOT NULL,

    PRIMARY KEY ("id")
);
