-- CreateTable
CREATE TABLE "Ia" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "rank" INTEGER NOT NULL,
    "stableCoin" VARCHAR(6) NOT NULL,
    "coinNumber" INTEGER NOT NULL DEFAULT 1,
    "spread" INTEGER NOT NULL DEFAULT 1,
    "profitPercent" DOUBLE PRECISION NOT NULL,
    "time" INTEGER NOT NULL,
    "lossPercent" DOUBLE PRECISION NOT NULL,
    "loop" INTEGER NOT NULL DEFAULT 1,
    "amount" DECIMAL(65,10) NOT NULL,

    PRIMARY KEY ("id")
);
