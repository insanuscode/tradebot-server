/*
  Warnings:

  - Added the required column `userId` to the `Ia` table without a default value. This is not possible if the table is not empty.

*/
-- CreateEnum
CREATE TYPE "Role" AS ENUM ('USER', 'ADMIN');

-- AlterTable
ALTER TABLE "Ia" ADD COLUMN     "userId" TEXT NOT NULL;

-- CreateTable
CREATE TABLE "Login" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updadtedAt" TIMESTAMP(3) NOT NULL,
    "email" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "name" TEXT,
    "role" "Role" NOT NULL DEFAULT E'USER',
    "isActivate" BOOLEAN NOT NULL DEFAULT true,

    PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Login.email_unique" ON "Login"("email");

-- AddForeignKey
ALTER TABLE "Ia" ADD FOREIGN KEY ("userId") REFERENCES "Login"("id") ON DELETE CASCADE ON UPDATE CASCADE;
