const { PrismaClient } = require('@prisma/client');
const getBookTicker = require('../src/util/getBookTicker');
const getExchangeInfo = require('../src/util/getExchangeInfo');

(async function updateMeta() {
  const prisma = new PrismaClient();
  try {
    const eInfo = await getExchangeInfo();
    const book = await getBookTicker();

    const blackList = ['BUSDUSDT', 'USDCUSDT', 'DAIUSDT'];
    let left = book.map((d) => d.symbol).filter((c) => !blackList.includes(c));
    left = JSON.stringify(left);

    let right = eInfo.map((d) => d.quoteAsset);
    right = Array.from(new Set(right));
    right = JSON.stringify(right);

    const validPairs = 'VALID_PAIRS';
    const rightCoinMeta = 'RIGHT_SIDE_COINS';

    await prisma.meta.upsert({
      where: { description: validPairs },
      update: { value: left },
      create: { description: validPairs, value: left },
    });

    await prisma.meta.upsert({
      where: { description: rightCoinMeta },
      update: { value: right },
      create: { description: rightCoinMeta, value: right },
    });
  } catch (err) {
    console.log(err);
  } finally {
    prisma.$disconnect();
  }
})();
